package com.epam.arrays.model;

import java.util.Arrays;

public class ArraysManipulation {

  private int[] getUniqueIntArray(int[] array) {
    int[] distinctElements = {};
    int newArrLength = distinctElements.length;
    for (int anArray : array) {
      boolean exists = false;
      if (distinctElements.length == 0) {
        distinctElements = new int[1];
        distinctElements[newArrLength] = anArray;
        newArrLength++;
      } else {
        for (int distinctElement : distinctElements) {
          if (anArray == distinctElement) {
            exists = true;
            break;
          }
        }
        if (!exists) {
          distinctElements = Arrays.copyOf(distinctElements, distinctElements.length + 1);
          distinctElements[distinctElements.length - 1] = anArray;
          newArrLength++;
        }
      }
    }
    return distinctElements;
  }

  public int[] getArrayWithCommonMembers(int[] array1, int[] array2) {
    if (array1.length > array2.length) {
      int[] temp = array1;
      array1 = array2;
      array2 = temp;
    }
    array1 = getUniqueIntArray(array1);
    int[] temp = new int[array1.length];
    int count = 0;
    for (int anArray1 : array1) {
      for (int anArray2 : array2) {
        if (anArray1 == anArray2) {
          temp[count] = anArray1;
          count++;
          break;
        }
      }
    }
    return Arrays.copyOf(temp, count);
  }

  public int[] getArrayWithDistinctMembers(int[] array1, int[] array2) {
    array1 = getUniqueIntArray(array1);
    array2 = getUniqueIntArray(array2);
    int[] temp = new int[array1.length + array2.length];
    int count = 0;
    for (int anArray1 : array1) {
      boolean isDistinct = true;
      for (int anArray2 : array2) {
        if (anArray1 == anArray2) {
          isDistinct = false;
          break;
        }
      }
      if (isDistinct) {
        temp[count] = anArray1;
        count++;
      }
    }
    for (int anArray2 : array2) {
      boolean isDistinct = true;
      for (int anArray1 : array1) {
        if (anArray2 == anArray1) {
          isDistinct = false;
          break;
        }
      }
      if (isDistinct) {
        temp[count] = anArray2;
        count++;
      }
    }
    return Arrays.copyOf(temp, count);
  }

  public int[] getArrayWithoutRepeatingMembers(int[] array) {
    int count = 0;
    int[] temp = new int[array.length];
    for (int anArray : array) {
      int countRepeats = 0;
      for (int anArray1 : array) {
        if (anArray1 == anArray) {
          countRepeats++;
        }
      }
      if (countRepeats <= 2) {
        temp[count] = anArray;
        count++;
      }
    }
    return Arrays.copyOf(temp, count);
  }

  public int[] getArrayWithoutSeries(int[] array) {
    int[] temp = new int[array.length];
    int count = 0;
    for (int i = 0; i < array.length; i++) {
      int countRepeats = 0;
      for (int j = i + 1; j < array.length; j++) {
        if (array[j] == array[i]) {
          countRepeats++;
        } else {
          break;
        }
      }
      temp[count] = array[i];
      count++;
      if (countRepeats > 0) {
        i += countRepeats;
      }
    }
    return Arrays.copyOf(temp, count);
  }
}