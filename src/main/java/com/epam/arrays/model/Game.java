package com.epam.arrays.model;

public class Game {

  private int heroPower;

  public Game() {
    heroPower = 25;
  }

  public int getHeroPower() {
    return heroPower;
  }

  private int getArtifact() {
    return (int) (10 + 71 * Math.random());
  }

  private int getMonsterPower() {
    return (int) (-(5 + 96 * Math.random()));
  }

  private int throwCube() {
    return (int) (2 * Math.random());
  }

  public int[] createRoom() {
    int[] room = new int[10];
    for (int i = 0; i < room.length; i++) {
      room[i] = (throwCube() == 0) ? getArtifact() : getMonsterPower();
    }
    return room;
  }

  public int getTheStrongestMonsterRoom(int[] room) {
    int theStrongestMonster = room[0];
    for (int aRoom : room) {
      if (aRoom < theStrongestMonster) {
        theStrongestMonster = aRoom;
      }
    }
    return theStrongestMonster;
  }

  public String[] getGameMessages() {
    String[] gameMessages = new String[6];
    gameMessages[0] = "This is model of rooms(- means monster power, + power of artifact)";
    gameMessages[1] = "Now You can defeat the strongest monster with power";
    gameMessages[2] = "and save the princess. Good luck";
    gameMessages[3] = "You must enter room number ";
    gameMessages[4] = "to find artifact that raise your power points on ";
    gameMessages[5] = "It's not your day. Monster doesn't give you any chance\n";
    return gameMessages;
  }
}
