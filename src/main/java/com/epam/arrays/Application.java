package com.epam.arrays;

import com.epam.arrays.controller.Controller;

public class Application {

  public static void main(String[] args) {
    new Controller().execute();
  }
}
