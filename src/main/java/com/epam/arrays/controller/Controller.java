package com.epam.arrays.controller;

import com.epam.arrays.model.ArraysManipulation;
import com.epam.arrays.model.Game;
import com.epam.arrays.view.View;

public class Controller {

  private View view;
  private ArraysManipulation manipulation;
  private Game game;

  public Controller() {
    view = new View();
    manipulation = new ArraysManipulation();
    game = new Game();
  }

  public void execute() {
    view.printMenu();
    while (true) {
      int menuItem = view.enterMenuItem(6);
      if (menuItem == 0) {
        executeItem0();
      } else if (menuItem == 1) {
        executeItem1();
      } else if (menuItem == 2) {
        executeItem2();
      } else if (menuItem == 3) {
        executeItem3();
      } else if (menuItem == 4) {
        executeItem4();
      } else {
        break;
      }
    }
  }

  private void executeItem0() {
    int[] array1 = view.enterArray(view.enterArraySize());
    int[] array2 = view.enterArray(view.enterArraySize());
    view.printMessage(view.getMenuMessages()[0]);
    view.printArray(manipulation.getArrayWithCommonMembers(array1, array2));
    view.printMenu();
  }

  private void executeItem1() {
    int[] array1 = view.enterArray(view.enterArraySize());
    int[] array2 = view.enterArray(view.enterArraySize());
    view.printMessage(view.getMenuMessages()[1]);
    view.printArray(manipulation.getArrayWithDistinctMembers(array1, array2));
    view.printMenu();
  }

  private void executeItem2() {
    int[] array = view.enterArray(view.enterArraySize());
    view.printMessage(view.getMenuMessages()[2]);
    view.printArray(manipulation.getArrayWithoutRepeatingMembers(array));
    view.printMenu();
  }

  private void executeItem3() {
    int[] array = view.enterArray(view.enterArraySize());
    view.printMessage(view.getMenuMessages()[2]);
    view.printArray(manipulation.getArrayWithoutSeries(array));
    view.printMenu();
  }

  private void executeItem4() {
    int[] room = game.createRoom();
    view.printMessage(game.getGameMessages()[0]);
    view.printArray(room);
    int heroPower = game.getHeroPower();
    for (int i = 0; i < room.length; i++) {
      if (room[i] > 0) {
        view.printMessage(String
            .format("%s%d %s%d", game.getGameMessages()[3], i, game.getGameMessages()[4],
                room[i]));
        heroPower += room[i];
      }
      if (Math.abs(heroPower) >= Math.abs(game.getTheStrongestMonsterRoom(room))) {
        view.printMessage(String
            .format("%s %d %s", game.getGameMessages()[1], game.getTheStrongestMonsterRoom(room),
                game.getGameMessages()[2]));
        break;
      }
    }
    if (Math.abs(heroPower) < Math.abs(game.getTheStrongestMonsterRoom(room))) {
      view.printMessage(game.getGameMessages()[5]);
    }
    view.printMenu();
  }
}