package com.epam.arrays.view;

import java.util.Arrays;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {

  private Scanner input = new Scanner(System.in);
  private Logger logger = LogManager.getLogger(View.class);
  private String[] menuMessages;

  public View() {
    menuMessages = new String[3];
    menuMessages[0] = "Numbers present in both arrays are ";
    menuMessages[1] = "Numbers present in one array are ";
    menuMessages[2] = "Result is ";
  }

  private int enterInteger() {
    int integer;
    while (true) {
      if (input.hasNextInt()) {
        integer = input.nextInt();
        break;
      } else {
        logger.error("Not integer! Try again: ");
        input.nextLine();
      }
    }
    return integer;
  }

  public int enterArraySize() {
    logger.info("Please enter the size of array: ");
    return enterInteger();
  }

  public int enterMenuItem(int finishValue) {
    int menuItem;
    while (true) {
      menuItem = enterInteger();
      if ((menuItem >= 0) && (menuItem < finishValue)) {
        break;
      } else {
        logger.info("Non-existent menu item. Try again: ");
        input.nextLine();
      }
    }
    return menuItem;
  }

  public int[] enterArray(int arraySize) {
    int[] array = new int[arraySize];
    logger.info("Please fill in the array: ");
    for (int i = 0; i < arraySize; i++) {
      array[i] = enterInteger();
    }
    printArray(array);
    return array;
  }

  public String[] getMenuMessages() {
    return menuMessages;
  }

  public void printMenu() {
    logger.info("Menu:\n" + "To find numbers present in both arrays press 0\n"
        + "To find numbers present in one array press 1\n"
        + "To delete in array all numbers that are repeated more than two times press 2\n"
        + "To find in the array all series of identical elements that are in succession, and "
        + "remove all elements from one element except one press 3\n"
        + "To simulate game press 4\n"
        + "To exit press 5\n"
        + "Please make your choice: ");
  }

  public void printMessage(String message) {
    logger.info(message);
  }

  public void printArray(int[] array) {
    logger.info(Arrays.toString(array));
  }
}
